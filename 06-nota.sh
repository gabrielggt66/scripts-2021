
#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# Febrer 2022
# Validar nota: suspès, aprovat,notable,excelent
# -------------------------------
#suspes,aprovat,notable,excelent
# 1) si num args no es correcte plegar
if [ $# -ne 1 ]
then
  echo "Error numero arguments incorrecte"
  echo "Usage: $0 nota"
  exit $ERR_NARGS
fi	
# 2) Validar rang nota
if ! [ $1 -ge 0 -a $1 -le 10 ]
then
  echo "Error nota $1 no valida"
  echo "nota pren valors de 0 a 10"
  echo "Usage: $0 nota"
  exit $ERR_NOTA
fi
#3)xixa
nota=$1
if [ $nota -ge 0 -a $nota -lt 5 ]; then
  echo "La nota $nota es suspes"

elif [  $nota -lt 7 ]; then
  echo "La nota $nota es aprovat"

elif [  $nota -lt 9 ]; then
  echo "La nota $nota es notable"

else
  echo "La nota $nota es excelent"
fi
exit 0
