#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# Febrer 2022
# Descripcio:exemples bucle for
# ------------------------------
#8)Llistar les logins numerats

comptador=1
llista_nom=$(cut -d: -f1 /etc/passwd | sort)

for elem in $llista_nom
do 
  echo "$comptador: $elem"
 ((comptador++))
done
exit 0
# 7)Listar numerats els noms dels files del directori actiu
comptador=1
llista_nom=$(ls)

for elem in $llista_nom
do 
  echo "$comptador: $elem"
 ((comptador++))
done
exit 0



# iterar per la llista d'arguments
comptador=1

for arg in $*
do 
  echo "$comptador: $arg"
  comptador=$((comptador+1))
done
exit 0

#“$@” expandeix els words encara que estiguin encapsulats
# iterar per la llista d'arguments
for arg in "$@"
do 
  echo $arg
done
exit 0

# iterar per la llista d'arguments
for arg in $*
do 
  echo $arg
done
exit 0
# iterar pel valor d'una variable
llistat=$(ls)
for nom in $llistat
do
  echo $nom
done
exit 0

# iterar per un conjunt d'elements

for nom in "pere pau marta anna"
do
  echo "$nom"
done
exit 0

# iterar per un conjunt d'elements

for nom in "pere" "pau" "marta" "anna"
do
  echo "$nom"
done
exit 0
