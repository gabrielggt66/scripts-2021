#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# Febrer 2022
# Descripcio:
# ------------------------------

#xixa
while read -r line
do
   
    cognom=$(echo $line | cut -d' ' -f2)
    inicial=$(echo $line | cut -c1)
    echo "$inicial.$cognom"
done
exit 0