#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# Febrer 2022
# Descripcio:
# ------------------------------
ERR_NARGS=1

#1) Validar existeix un arg
if  [ $# -lt 1 ]
then
  echo "Error, numero d'arguments no valid"
  echo "Usage: $0 maxim"
  exit 1
fi
#xixa
llista_arguments=$*
caracters=3
comptador_paraules=0
for elem in $llista_arguments
do
	num_caracters=$(echo $elem | wc -c)
	if [ $num_caracters -ge $caracters ]
	then
		((comptador_paraules++))
	fi
done
echo "Hi ha $comptador_paraules paraules amb 3 o mes caracters"
exit 0