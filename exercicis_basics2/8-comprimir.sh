#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# Febrer 2022
# Descripcio:
# ------------------------------


#1) Validar existeix un arg
if  [ $# -lt 1 ]
then
  echo "Error, numero d'arguments no valid"
  echo "Usage: $0 maxim"
  exit 1
fi

if [ $1=='-h' -o $1=='--help' ]
then
  echo "Usage: prog file file2 file3 ..."
  exit 0
fi
#xixa


llista_arguments=$*
quantitat_files=0
salida=0
for file in $llista_arguments
do
        if ! [ -f $file ]
        then
            echo "El file $file no existeix" 1>&2
        else
        gzip $file
        
            if [ $? -eq 0 ]
            then
            echo "El file $file s'ha comprimit correctament"
            ((quantitat_files++))
            else
            echo "Error: El file $file no s'ha pogut imprimir" 1>&2
            salida=2
            fi
            fi
done
echo "Comprimits: $quantitat_files"
exit $salida
