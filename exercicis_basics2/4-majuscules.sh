#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# Febrer 2022
# Descripcio:
# ------------------------------



#xixa
comptador=1
while read -r line
do
    majuscules=$(echo "$line "| tr [:lower:] [:upper:])
    echo "$comptador: $majuscules"
    ((comptador++))
done
exit 0