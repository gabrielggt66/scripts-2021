#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# Febrer 2022
# Descripcio:
# ------------------------------
ERR_NARGS=1

#1) Validar existeix un arg
if  [ $# -lt 1 ]
then
  echo "Error, numero d'arguments no valid"
  echo "Usage: $0 matricula"
  exit 1
fi

llista_matricules=$*
comptador_error=0
for matricula in $llista_matricules
do
    echo "$matricula" | grep -E "^[0-9]{4}-[A-Z]{3}$"

if [ $? -eq 0 ]
then
echo "Matricula $matricula valida"

else
    echo " Error: matricula $matricula no valida" 1>&2
    ((comptador_error++))
fi
done
exit $comptador_error 