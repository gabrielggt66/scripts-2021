#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# Febrer 2022
# per cada nota diu si suspes, aprovat ,not ,e
# ------------------------------

ERR_NARGS=1
ERR_NODIR=2
ERR_NOREGULARFILE=3
# 1) si num args no es correcte plegar
if [ $# -lt 2 ]
then
  echo "Error numero arguments incorrecte"
  echo "Usage: $0 file[...] dir-desti "
  exit $ERR_NARGS
fi

desti= $(echo $* | sed 's/^.* //')
llista_file=$(echo $* | sed 's/ [^ ]*$//')


#3) 2n arg es un dir
if ! [ -d $desti ]
then
	echo "Error: $desti no es un directori"  1>&2 
	echo "Usage: $desti [...] dir-desti  "
 	exit $ERR_NOREGULARFILE

fi



for elem in $llista_files
do
	if ![ -f $file ]
	then 
		echo "Error: $file no es un regular file" 
		echo "Usage: $file [...] dir-desti  "
        	exit $ERR_NODIR
	else
		cp $file $desti
fi
done
exit 0
