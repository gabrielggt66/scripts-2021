#! /bin/bash
#@gabrielggt66 M01-ISO
#Febbrer 2022
#Descripcio:
#----------------------------------------------------------
#1) Validem arguments
if [ $# -eq 0 ]
then
	echo "Error: numero d'arguments incorrecte"
	echo "Usage:  $0 [-a -b -c -d -e -f] arg[...]"
        exit 1	
fi

opcions=""
arguments=""
fitxer=""
num=""
while [ "$1" ]
do
	case "$1" in
	"-b"|"-c"|"-e")
	opcions="$opcions $1";;
	"-a")
	opcions="$opcions $1"
	fitxer=$2
	shift;;
	"-d")
	opcions="$opcions $1"
	num=$2
	shift;;
	*)
	arguments="$arguments $1";;
	esac
	shift
done
echo "Opcions: $opcions"
echo "Arguments: $arguments"
echo "Fitxer: $fitxer"
echo "Num: $num"
exit 0