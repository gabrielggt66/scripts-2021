# /bin/bash
#@ CURS 2021-2022
#ASIX M01-ISO
#Exemple de funcions

function hola(){
	echo "hola"
	return 0
}

function dia(){
	date
	return 0
}

function suma(){
	echo $(($1+$2))
	return 0
}

function all(){
	hola
	echo "avui som : $(dia)"
	suma 6 9
}

function fsize(){
	#1)validar existeix login
	login=$1
	linia=$(grep "^$login:" /etc/passwd)
	if [ -z "$linia" ] ; then
		echo "Error: user $login inexistent"
		return 1
	fi
	#2)obtenir el seu home-dir
	dirHome=$(echo $linia | cut -d: -f6)
	#3)calcular el du -sh
	du -sh $dirhome
	return 0
}

function loginargs(){
	#1)validar almenys es rep 1 login
	if [ $# -eq 0 ]; then
		echo "error..."
		return 1
	fi
	#2)iterar per cada login
	for login in $*
	do
		fsize $login
	done
}
#3)loginfile
#Rep com a argument un nom de fitxer que conte
#un login per linia. Mostrar l'ocupacio de disc
#de cada usuari usant fsize .Verificar que es
#rep u argumetn i que es un regular file.

function loginfile(){

	if [ -f "$1" ];then
		echo "fitxer inexistent"
		return 1
	fi
	filein=$1
	while read -r login
	do
		fsize $login
	done < $filein
}

#4)	loginboth
#  	loginboth [file]
#  processa file o stdin,mostra fsize
#  dels users
function loginboth(){
	filein=/dev/stdin
	if [ $# -eq 1 ]; then
		filein=$1
	fi
	while read -r login
	do
		fsize $login
	done < $filein
}
#5) grepid gid
#validar rep es un arg
#validar que es un gid valid
#retorna la llista de logins que tenen
#aquest grup com a grup principal
function grepid(){
	#validar rep es un arg
	if [ $# -ne 1 ]
	then
		echo "Error: Nombre de arguments insuficient"
		return 1
	fi
	#validar que es un gid valid
	GID=$1
	grep -q "^[^:]*:[^:]*:$GID:" /etc/group 
	
	if [ $? -ne 0 ]
	then
		echo "Error: gid $GID inexistent"
		return 2
	fi
	#retorna la llista de logins que tenen
	logs_id=$(cut -d: -f1,4 /etc/passwd | grep ":$gid$" | cut -d: -f1)
	echo "$logs_id"
	

}
#6) gidsize gid
#Donat un GID com argument, mostrar 
#per a cada ususari que pertany a aquest 
#grup l'ocupacio de disc del seu home 

function gidsize(){
	gid=$1
	llista_logins=$(grepid $gid)
	for login in $llista_logins
	do 
		fsize $login	
	done
}

#7)allgidsize
#Informe de tots els gids llistant
#l'ocupacio dels usuaris que hi pertanyen
function allgidsize(){
	llista_gids=$(cut -d: -f4 /etc/passwd | sort -gu)
	for gid in $llista_gids
	do
		echo "GID: $gid-------------"
		gidsize $gid
	done
}

#8) filterGroup
#llistar linies del /etc/passwd
#del usuaris dels grups 0 al 100
function filterGroup(){
	file=passwd.txt
	while read -r line
	do
		gid=$(echo $line | cut -d: -f4 )
		if [ $gid -ge 0 -a $gid -le 100 ]
		then
			echo $line
		fi
	done < $file	
}
#9)fstype
#Donat un fstype d'argument  llista el device
#i el mountpoint (per odre de device) de les entrades de fstab d'quest fstype.function fstype(){
	fstype=$1
}

#10)allfstype 
#LLista per a cada fstype que existeix al fstab (per ordre lexicogràfic)
#les entrades d'quest tipus. Llistar tabuladament el device i el mountpoint.

