#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# Febrer 2022
# Descripcio:comptador de zero fins al valor indicat
# ------------------------------
ERR_NARGS=1
ERR_ARGVL=2
#1) Validar existeix un arg
if [ $# -lt 1 ]
then
  echo "Error, numero d'arguments no valid" 1>&2
fi
#2) Validar arg pren valors [1-12]
mesos_anys=$*
for mes in $mesos_anys
do
	if ! [ $mes -ge 1 -a $mes -le 12 ]
	then
  	echo "Error, mes $mes no vàlid" 1>&2
	
#3)Xixa
        else
	case $mes in 
		"2")
		dies_mes=28;;
	"4"|"6"|"9"|"11")
		dies_mes=30;;
	*)
		dies_mes=31;;
	esac
	echo "El mes $mes te $dies_mes dies"
	fi
done
exit 0



