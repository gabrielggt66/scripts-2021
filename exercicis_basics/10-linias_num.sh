#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# Febrer 2022
# Descripcio:
# ------------------------------
ERR_NARGS=1

#1) Validar existeix un arg
if ! [ $# -eq 1 ]
then
  echo "Error, numero d'arguments no valid"
  echo "Usage: $0 maxim"
  exit 1
fi

#2)xixa
maxim=$1
comptador=1

while read -r line
do
  
  echo "$comptador: $line"
    if [ $comptador -eq $maxim ]
    then
      exit 0
    fi
    ((comptador++))
done
exit 0