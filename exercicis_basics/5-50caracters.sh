#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# Febrer 2022
# Descripcio:entrada estandard retallant els primers 50 caracters
# ------------------------------
#1)xixa

while read -r line
do
	echo "$line" | cut -c1-50 
	
done
exit 0

