#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# Febrer 2022
# Descripcio:comptador de zero fins al valor indicat
# ------------------------------
# 1) si num args no es correcte plegar
if [ $# -gt 1 ]
then
  echo "Error numero arguments incorrecte"
  echo "Usage: $0 arg"
  exit $ERR_NARGS
fi
#Xixa
max=$1
comptador=0
while [ $comptador -le $max ]
do
	echo $comptador
	((comptador++))
done
exit 0


