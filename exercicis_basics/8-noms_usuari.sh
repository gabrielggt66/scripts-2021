#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# Febrer 2022
# Descripcio:
# ------------------------------
ERR_NARGS=1
# 1) si num args no es correcte plegar
if [ $# -lt 1 ]
then
  echo "Error numero arguments incorrecte"
  echo "Usage: $0 arg"
  exit $ERR_NARGS
fi
#2)xixa
llista_noms=$*
for nom in $llista_noms
do
	 grep -q  "^$nom:"  /etc/passwd
	if [ $? -eq 0 ] ;then
		echo "El nom '$nom' es al sistema"
	else
		echo "Error: el nom '$nom'no apareix" 1>&2
	fi
done
exit 0
