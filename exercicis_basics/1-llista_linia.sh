#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# Febrer 2022
# Descripcio:entrada estandard numerat linia a linia
# ------------------------------
#1)xixa
num=1
while read -r line
do
	echo "$num: $line"
	((num++))
done
exit 0

