#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# Febrer 2022
# Descripcio:
# ------------------------------
ERR_NARGS=1

# 1) si num args no es correcte plegar
if [ $# -lt 1 ]
then
  echo "Error numero arguments incorrecte"
  echo "Usage: $0 arg"
  exit $ERR_NARGS
fi
#xixa
arguments=$*
num=1
for elem in $arguments
do
	echo "$num: $elem"
	((num++))
done
exit 0


