#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# Febrer 2022
# per cada nota diu si suspes, aprovat ,not ,ex
# ------------------------------
ERR_NARGS=1
ERR_NOTA=2
# 1) si num args no es correcte plegar
if [ $# -lt 1 ]
then
  echo "Error numero arguments incorrecte"
  echo "Usage: $0 "
  exit $ERR_NARGS
fi

#2) validar nota [0-10]
for nota in $*
do
if ! [ $nota -ge 0 -a $nota -le 10 ]
then
  echo "Error nota $nota no valida [0-10]" >> /dev/stderr

#3)xixa
elif [  $nota -lt 5 ]; then
  echo "La nota $nota es suspes"

elif [  $nota -lt 7 ]; then
  echo "La nota $nota es aprovat"

elif [  $nota -lt 9 ]; then
  echo "La nota $nota es notable"

else
  echo "La nota $nota es excelent"
fi
done
exit 0


