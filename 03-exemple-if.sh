#! /bin/bash
#@gabrielggt66 M01-ISO
#Febbrer 2022
#Exemple if:indica si es major d'edat
#  $ prog edat
#----------------------------------------------------------
#1) Validem arguments
if [ $# -ne 1 ]
then
	echo "Error: numero d'arguments incorrecte"
	echo "Usage:  $0 edat"
       exit 1	
fi

#2) Xixa
edat=$1
if  [ $edat -ge 18 ]
then
  echo "edat $edat es major d'edat"

else
  echo "edat $edat es menor d'edat"

fi

exit 0

