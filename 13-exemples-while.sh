#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# Febrer 2022
# Descripcio:exemples bucle while
# ------------------------------





#7)numerar stdin linia a linia i passar a majuscules
num=1
while read -r line
do
	echo "$num: $line" | tr 'a-z' 'A-Z'
	((num++))
done
exit 0



#6)processar stdin fins al token FI
read -r line	
while ["$line" != "FI"]
do
	echo " $line"
	read -r line	
done
exit 0

#5) numerar stdin linia a  linia
num=1
while read -r line
do
	echo "$num: $line"
	((num++))
done
exit 0




#4) Processar l'entrada estandard
while read -r line
do
	echo $line
done
exit 0


#3)Iterar arguments amb shift
while [ -n "$1" ]
do
	echo"$1, $# , $*"
	shift
done
exit 0




#comptador decreixent del arg [N-0]
Min=0
comptador=$1
while [ $comptador -ge 0 ]
do
	echo $comptador
	((comptador--))
done
exit 0

#Mostrar un comptador del 1 a Max
Max=10
comptador=1
while [ $comptador -le $Max ]
do
	echo $comptador
	((comptador++))
done
exit 0

