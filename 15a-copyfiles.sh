#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# Febrer 2022
# per cada nota diu si suspes, aprovat ,not ,e
# ------------------------------

ERR_NARGS=1
ERR_NODIR=2
ERR_NOREGULARFILE=3
# 1) si num args no es correcte plegar
if [ $# -ne 2 ]
then
  echo "Error numero arguments incorrecte"
  echo "Usage: $0 file[...] dir-desti "
  exit $ERR_NARGS
fi

file=$1
desti=$2
#2)1r arg es un file
if ![ -f $file ]
then 
	echo "Error: $file no es un regular file" 
	echo "Usage: $file [...] dir-desti  "
        exit $ERR_NODIR
fi

#3) 2n arg es un dir
if ![ -d $2 ]
then
	echo "Error: $desti no es un directori"  1>&2 
	echo "Usage: $desti [...] dir-desti  "
 	 exit $ERR_NOREGULARFILE

fi
cp $file $desti
