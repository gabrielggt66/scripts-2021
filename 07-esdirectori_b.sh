#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# Febrer 2022
# Validar directori: suspès, aprovat
# -------------------------------
#si no es arg error
#si no es un dir 
#si es un dir --> llistar-lo

ERR_NARGS=1
ERR_DIR=2
#  si num args no es correcte plegar
if [ $# -ne 1 ]
then
  echo "Error numero arguments incorrecte"
  echo "Usage: $0 nota"
  exit $ERR_NARGS
fi
# si es demana help mostrar i plegar
if [ "$1" = "-h" -o "$1" = "--help" ]; then
  echo "Programa: $0 dir"
  echo "Author: @edt"
  echo "that's all folks"
  exit 0
fi	

#si no es un dir
if  [ -d $1 != 0  ]; then
  echo "Error: $dir no es un directori"
  echo "Usage: $0 directori"
  exit $ERR_DIR
fi
#xixa
dir=$1
ls $dir


