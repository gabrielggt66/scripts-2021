#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# Febrer 2022
# Descripcio:dir es un directori,fer un ls del directori
# ------------------------------
ERR_NARGS=1
ERR_DIR=2
# 1) si num args no es correcte plegar
if [ $# -ne 1 ]
then
  echo "Error numero arguments incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

#2)si no es un dir
if  ! [ -d $1  ]; then
  echo "Error: $dir no es un directori"
  echo "Usage: $0 directori"
  exit $ERR_DIR
fi
#3)xixa

num=1
dir=$1
llista_ls=$(ls $dir)

for elem in $llista_ls
do
	echo "$num: $elem"
	((num++))
done
exit 0
