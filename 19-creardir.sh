#! /bin/bash
#@gabrielggt66 M01-ISO
#Febbrer 2022
#Descripcio:Crear n directoris
#almenys 1 arg
#mkdir no genera cap sortida
#0 tot dir creats ok
#1 error en el numero d'arguments
#2 si algun dir no s'ha pogut crear
#----------------------------------------------------------
ERR_NARGS=1
ERR_MKDIR=2
status=0
#1) Validem arguments
if [ $# -lt 1 ]
then
	echo"Error: numero args incorrecte"
	echo "usage: $0 nomdir[...]"
        exit 1
fi

llista_directoris=$*
for dir in $llista_directoris
do
	mkdir $dir &> /dev/null
	if [ $? -ne 0 ];then
		echo "Error:no s'ha creat $dir" >&2
	status=$ERR_MKDIR
	fi

done
exit $status
