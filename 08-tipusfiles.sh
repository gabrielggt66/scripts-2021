#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# Febrer 2022
# Validar directori: suspès, aprovat
# -------------------------------
#regular file,dir,link
#validacions ...

ERR_NARGS=1

# 1) si num args no es correcte plegar
if [ $# -ne 1 ]
then
  echo "Error numero arguments incorrecte"
  echo "Usage: $0 "
  exit $ERR_NARGS
fi
#xixa
fit=$1
if  [-f $fit ]; then
  echo "$fit es un regular file"
elif [ -h $fit ]; then
  echo "$fit es un link"
elif [ -d $fit ]; then
  echo "$fit es un directori"
else
  echo"Es una altra cosa"
fi
exit 0
